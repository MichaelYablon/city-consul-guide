# How to Run

This is a step-by-step guide how to run the example:

## Installation

* The example is implemented in Java. 

## Build

Change to the directory `city-guide` and run `./mvnw clean
package` or `mvnw.cmd clean package` (Windows). This will take a while:

```
[~/microservice-consul/microservice-consul-demo]./mvnw clean package
....
[INFO] --- spring-boot-maven-plugin:2.1.9.RELEASE:repackage (repackage) @ city-guide-calc ---
[INFO] Replacing main artifact with repackaged archive
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary for city-guide 0.0.1-SNAPSHOT:
[INFO]
[INFO] city-guide ......................................... SUCCESS [  0.928 s]
[INFO] city-guide-catalog ................................. SUCCESS [ 11.309 s]
[INFO] city-guide-calc .................................... SUCCESS [  4.779 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  17.438 s
[INFO] Finished at: 2019-10-29T15:14:34+02:00
[INFO] ------------------------------------------------------------------------
```

## Run the containers

First you need to build the Docker images. Change to the directory
`docker` and run `docker-compose build`. This will download some base
images, install software into Docker images and will therefore take
its time:

```
[~/city-guide/docker]docker-compose build 
....
Removing intermediate container 1d59f8227b12
Step 4/4 : EXPOSE 8081
 ---> Running in 11e7fbacfa01
 ---> 9cfa7772986f
Removing intermediate container 11e7fbacfa01
Successfully built 9cfa7772986f
```

Now you can start the containers using `docker-compose up -d`. The
`-d` option means that the containers will be started in the
background and won't output their stdout to the command line:

Check wether all containers are running:

```
C:\Users\Mykhailo_Yablon\IdeaProjects\consul-city-guide\city-guide>docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                                                    NAMES
12f246092254        docker_catalog      "/bin/sh -c '/usr/bi…"   4 minutes ago       Up 4 minutes        0.0.0.0:8080->8080/tcp                                                                   docker_catalog_1
ff16285616f4        docker_calc         "/bin/sh -c '/usr/bi…"   4 minutes ago       Up 4 minutes        0.0.0.0:8081->8081/tcp                                                                   docker_calc_1
ccc923d83852        neo4j:latest        "/sbin/tini -g -- /d…"   4 minutes ago       Up 4 minutes        0.0.0.0:7474->7474/tcp, 7473/tcp, 0.0.0.0:7687->7687/tcp                                 docker_neo4j_1
276442d37bfe        consul:latest       "docker-entrypoint.s…"   4 minutes ago       Up 4 minutes        8300-8302/tcp, 8600/tcp, 8301-8302/udp, 0.0.0.0:8500->8500/tcp, 0.0.0.0:8600->8600/udp   docker_consul_1

```

You can access the microservices at http://localhost:8080/ 
 and the Consul dashboard
at http://localhost:8500 .

You can terminate all containers using `docker-compose down`.