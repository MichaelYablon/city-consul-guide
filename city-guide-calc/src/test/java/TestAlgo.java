import com.city.guide.calc.model.Edge;
import com.city.guide.calc.model.Graph;
import com.city.guide.calc.model.Vertex;
import com.city.guide.calc.service.DijkstraAlgorithm;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

public class TestAlgo {
    private List<Vertex> nodes;
    private List<Edge> edges;

    @Test
    public void testExcute() {
        nodes = new ArrayList<Vertex>();
        edges = new ArrayList<Edge>();
        for (int i = 0; i < 11; i++) {
            Vertex location = new Vertex((long) i, "Node_" + i);
            nodes.add(location);
        }

        addLane(0L, 0, 1, 85);
        addLane(1L, 0, 2, 217);
        addLane(2L, 0, 4, 173);
        addLane(3L, 2, 6, 186);
        addLane(4L, 2, 7, 103);
        addLane(5L, 3, 7, 183);
        addLane(6L, 5, 8, 250);
        addLane(7L, 8, 9, 84);
        addLane(8L, 7, 9, 167);
        addLane(9L, 4, 9, 502);
        addLane(10L, 9, 10, 40);
        addLane(11L, 1, 10, 600);

        // Lets check from location Loc_1 to Loc_10
        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(nodes.get(8));
        LinkedList<Vertex> path = dijkstra.getPath(nodes.get(10));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }

    }

    private void addLane(Long laneId, long sourceLocNo, long destLocNo,
                         int duration) {
        Edge lane = new Edge(laneId, sourceLocNo, destLocNo, duration);
        edges.add(lane);
    }
}
