package com.city.guide.calc.service;

import com.city.guide.calc.model.Edge;
import com.city.guide.calc.model.Vertex;

import java.util.ArrayList;

public interface CatalogClientService {
    ArrayList<Edge> getEdges();

    ArrayList<Vertex> getVertices();
}
