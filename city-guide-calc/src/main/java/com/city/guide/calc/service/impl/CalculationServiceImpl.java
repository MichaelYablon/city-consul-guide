package com.city.guide.calc.service.impl;

import com.city.guide.calc.model.Edge;
import com.city.guide.calc.model.Graph;
import com.city.guide.calc.model.Vertex;
import com.city.guide.calc.service.CalculationService;
import com.city.guide.calc.service.CatalogClientService;
import com.city.guide.calc.service.DijkstraAlgorithm;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;

@Slf4j
@Service
@AllArgsConstructor
public class CalculationServiceImpl implements CalculationService {

    private final CatalogClientService catalogClientService;

    @Override
    public List<Vertex> getShortestWayByTime(Long sourceId, Long destinationId) {
        ArrayList<Vertex> nodes = catalogClientService.getVertices();
        List<Edge> edges = catalogClientService.getEdges();
        Vertex source = nodes.stream().filter(n -> n.id.equals(sourceId)).findFirst().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        Vertex destination = nodes.stream().filter(n -> n.id.equals(destinationId)).findFirst().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(source);
        LinkedList<Vertex> path = dijkstra.getPath(destination);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }
        return path;
    }

    @Override
    public List<Vertex> getShortestWayByConnections(Long sourceId, Long destinationId) {
        ArrayList<Vertex> nodes = catalogClientService.getVertices();
        List<Edge> edges = catalogClientService.getEdges();
        Vertex source = nodes.stream().filter(n -> n.id.equals(sourceId)).findFirst().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        Vertex destination = nodes.stream().filter(n -> n.id.equals(destinationId)).findFirst().orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        Graph graph = new Graph(nodes, edges);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(source);
        LinkedList<Vertex> path = dijkstra.getPath(destination);

        for (Vertex vertex : path) {
            System.out.println(vertex);
        }
        return path;
    }
}
