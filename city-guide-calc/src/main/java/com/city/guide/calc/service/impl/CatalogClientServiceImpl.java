package com.city.guide.calc.service.impl;

import com.city.guide.calc.model.Edge;
import com.city.guide.calc.model.Vertex;
import com.city.guide.calc.service.CatalogClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class CatalogClientServiceImpl implements CatalogClientService {
    private RestTemplate restTemplate;
    private String catalogUrl;

    @Autowired
    public CatalogClientServiceImpl(RestTemplate restTemplate,
                                    @Value("${catalog.service.port:8080}") long catalogServicePort) {
        this.restTemplate = restTemplate;
        this.catalogUrl = String.format("http://CATALOG/", catalogServicePort);
    }

    @Override
    public ArrayList<Edge> getEdges() {
        log.info("Getting Edges");
        log.info(catalogUrl);
        ResponseEntity<ArrayList<Edge>> responseEntity = restTemplate.exchange(catalogUrl + "routes", HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ArrayList<Edge>>() {
                });
        return responseEntity.getBody();
    }

    @Override
    public ArrayList<Vertex> getVertices() {
        log.info("Getting Edges");
        log.info(catalogUrl);
        ResponseEntity<ArrayList<Vertex>> responseEntity = restTemplate.exchange(catalogUrl + "cities", HttpMethod.GET,
                null,
                new ParameterizedTypeReference<ArrayList<Vertex>>() {
                });
        return responseEntity.getBody();
    }
}
