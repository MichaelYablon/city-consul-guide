package com.city.guide.calc.controller;

import com.city.guide.calc.model.Vertex;
import com.city.guide.calc.service.CalculationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("calculation")
@AllArgsConstructor
public class CalcController {

    private final CalculationService calculationService;

    @GetMapping("/time/source/{sourceId}/destination/{destinationId}")
    public List<Vertex> getShortestWayByTime(@PathVariable Long sourceId, @PathVariable Long destinationId) {
        return calculationService.getShortestWayByTime(sourceId, destinationId);
    }

    @GetMapping("/path/source/{sourceId}/destination/{destinationId}")
    public List<Vertex> getShortestWayByConnections(@PathVariable Long sourceId, @PathVariable Long destinationId) {
        return calculationService.getShortestWayByConnections(sourceId, destinationId);
    }
}
