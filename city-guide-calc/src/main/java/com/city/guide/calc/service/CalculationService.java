package com.city.guide.calc.service;

import com.city.guide.calc.model.Vertex;

import java.util.List;

public interface CalculationService {

    List<Vertex> getShortestWayByTime(Long sourceId, Long destinationId);

    List<Vertex> getShortestWayByConnections(Long sourceId, Long destinationId);
}
