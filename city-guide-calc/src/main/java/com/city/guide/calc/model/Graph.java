package com.city.guide.calc.model;

import lombok.Data;

import java.util.List;

@Data
public class Graph {
    private final List<Vertex> cities;
    private final List<Edge> edges;
}
