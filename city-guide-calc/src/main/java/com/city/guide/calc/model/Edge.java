package com.city.guide.calc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Edge {
    public Long id;
    public Long sourceCityId;
    public Long endCityId;
    public int timeToTravel;
}