package com.city.guide.catalog.dto;

import lombok.Data;

@Data
public class RouteDto {
    public Long id;
    public Long sourceCityId;
    public Long endCityId;
    public long timeToTravel;
}
