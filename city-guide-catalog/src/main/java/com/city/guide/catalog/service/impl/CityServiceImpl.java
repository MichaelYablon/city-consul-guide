package com.city.guide.catalog.service.impl;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.entity.neo.City;
import com.city.guide.catalog.mapper.CityMapper;
import com.city.guide.catalog.repository.neo.CityRepository;
import com.city.guide.catalog.service.CityService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@AllArgsConstructor
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;
    private final CityMapper cityMapper;

    @Override
    public List<CityDto> getCities() {
        return cityMapper.toDtos((List<City>) cityRepository.findAll());
    }

    @Override
    public CityDto getCity(Long id) {
        City city = getCityEntity(id);
        return cityMapper.toDto(city);
    }

    @Override
    public City getCityEntity(Long id) {
        return cityRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public CityDto addCity(CityDto cityDto) {
        City city = cityRepository.save(cityMapper.toEntity(cityDto));
        return cityMapper.toDto(city);
    }

    @Override
    public void deleteCity(Long id) {
        cityRepository.deleteById(id);
    }

    @Override
    public void deleteAllCities() {
        cityRepository.deleteAll();
    }
}
