package com.city.guide.catalog.service;

import com.city.guide.catalog.dto.RouteDto;

import java.util.List;

public interface RouteService {

    List<RouteDto> getRoutes();

    RouteDto getRoute(Long id);

    RouteDto addRoute(RouteDto cityDto);

    void deleteRoute(Long id);

    void deleteAllRoutes();
}
