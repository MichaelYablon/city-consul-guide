package com.city.guide.catalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CityGuideApplication {

    public static void main(String[] args) {
        SpringApplication.run(CityGuideApplication.class, args);
    }

}
