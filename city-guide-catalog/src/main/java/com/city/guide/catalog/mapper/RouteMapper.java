package com.city.guide.catalog.mapper;

import com.city.guide.catalog.dto.RouteDto;
import com.city.guide.catalog.entity.neo.Route;
import com.city.guide.catalog.service.CityService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = CityService.class)
public interface RouteMapper {

    @Mappings({
            @Mapping(source = "sourceCity.id", target = "sourceCityId"),
            @Mapping(source = "endCity.id", target = "endCityId")
    })
    RouteDto toDto(Route route);

    List<RouteDto> toDtos(List<Route> routes);

    @Mappings({
            @Mapping(source = "sourceCityId", target = "sourceCity"),
            @Mapping(source = "endCityId", target = "endCity")
    })
    Route toEntity(RouteDto routeDto);

    List<Route> toEntities(List<RouteDto> routeDtos);

}
