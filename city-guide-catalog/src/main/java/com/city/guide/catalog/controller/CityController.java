package com.city.guide.catalog.controller;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.service.CityService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("cities")
@AllArgsConstructor
public class CityController {

    private final CityService cityService;

    @GetMapping
    public List<CityDto> getCities() {
        return cityService.getCities();
    }

    @GetMapping("/{cityId}")
    public CityDto getCity(@PathVariable Long cityId) {
        return cityService.getCity(cityId);
    }

    @PostMapping
    public CityDto addCity(@RequestBody CityDto cityDto) {
        return cityService.addCity(cityDto);
    }

    @DeleteMapping("/{cityId}")
    public void deleteCity(@PathVariable Long cityId) {
        cityService.deleteCity(cityId);
    }

    @DeleteMapping
    public void deleteAllCities() {
        cityService.deleteAllCities();
    }
}
