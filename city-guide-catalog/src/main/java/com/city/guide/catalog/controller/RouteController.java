package com.city.guide.catalog.controller;

import com.city.guide.catalog.dto.RouteDto;
import com.city.guide.catalog.service.RouteService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("routes")
public class RouteController {

    private final RouteService routeService;

    @GetMapping
    public List<RouteDto> getRoutes() {
        return routeService.getRoutes();
    }

    @GetMapping("/{routeId}")
    public RouteDto getRoute(@PathVariable Long routeId) {
        return routeService.getRoute(routeId);
    }

    @PostMapping
    public RouteDto addRoute(@RequestBody RouteDto routeDto) {
        return routeService.addRoute(routeDto);
    }

    @DeleteMapping("/{routeId}")
    public void deleteCity(@PathVariable Long routeId) {
        routeService.deleteRoute(routeId);
    }

    @DeleteMapping
    public void deleteAllRoutes() {
        routeService.deleteAllRoutes();
    }
}
