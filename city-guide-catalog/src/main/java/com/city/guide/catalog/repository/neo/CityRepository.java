package com.city.guide.catalog.repository.neo;

import com.city.guide.catalog.entity.neo.City;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends Neo4jRepository<City, Long> {
}
