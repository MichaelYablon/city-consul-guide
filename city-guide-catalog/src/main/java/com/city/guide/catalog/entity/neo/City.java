package com.city.guide.catalog.entity.neo;

import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@NodeEntity
public class City {
    @Id
    @GeneratedValue
    public Long id;

    public String name;

    @Relationship(type = "ROUTE", direction = Relationship.INCOMING)
    Set<Route> routes;
}
