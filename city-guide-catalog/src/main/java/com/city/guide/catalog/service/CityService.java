package com.city.guide.catalog.service;

import com.city.guide.catalog.dto.CityDto;
import com.city.guide.catalog.entity.neo.City;

import java.util.List;

public interface CityService {

    List<CityDto> getCities();

    CityDto getCity(Long id);

    City getCityEntity(Long id);

    CityDto addCity(CityDto cityDto);

    void deleteCity(Long id);

    void deleteAllCities();
}
