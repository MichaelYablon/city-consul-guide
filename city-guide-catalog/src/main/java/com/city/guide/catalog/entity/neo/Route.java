package com.city.guide.catalog.entity.neo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@RelationshipEntity(type = "ROUTE")
public class Route {

    @Id
    @GeneratedValue
    public Long id;

    @StartNode
    public City sourceCity;

    @EndNode
    public City endCity;

    public long timeToTravel;
}
