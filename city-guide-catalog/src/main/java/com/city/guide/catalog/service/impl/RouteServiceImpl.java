package com.city.guide.catalog.service.impl;

import com.city.guide.catalog.dto.RouteDto;
import com.city.guide.catalog.entity.neo.City;
import com.city.guide.catalog.entity.neo.Route;
import com.city.guide.catalog.mapper.RouteMapper;
import com.city.guide.catalog.repository.neo.RouteRepository;
import com.city.guide.catalog.service.CityService;
import com.city.guide.catalog.service.RouteService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@AllArgsConstructor
public class RouteServiceImpl implements RouteService {

    private final RouteMapper routeMapper;
    private final RouteRepository routeRepository;
    private final CityService cityService;

    @Override
    public List<RouteDto> getRoutes() {
        return routeMapper.toDtos((List<Route>) routeRepository.findAll());
    }

    @Override
    public RouteDto getRoute(Long id) {
        return routeMapper.toDto(routeRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @Override
    public RouteDto addRoute(RouteDto routeDto) {
        Route route = routeRepository.save(routeMapper.toEntity(routeDto));
        return routeMapper.toDto(route);
    }

    @Override
    public void deleteRoute(Long id) {
        routeRepository.deleteById(id);
    }

    @Override
    public void deleteAllRoutes() {
        routeRepository.deleteAll();
    }
}
