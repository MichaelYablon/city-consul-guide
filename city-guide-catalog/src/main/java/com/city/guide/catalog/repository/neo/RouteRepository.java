package com.city.guide.catalog.repository.neo;

import com.city.guide.catalog.entity.neo.Route;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends Neo4jRepository<Route, Long> {
}
