package com.city.guide.catalog.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DataBaseConfig extends HikariDataSource {

        @Primary
        @Bean(name = "dataSource")
        public DataSource cityDataSource() {
            return new HikariDataSource(this);
        }
}
