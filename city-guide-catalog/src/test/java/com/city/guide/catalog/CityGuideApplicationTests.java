package com.city.guide.catalog;

import com.city.guide.catalog.repository.neo.CityRepository;
import com.city.guide.catalog.repository.neo.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
class CityGuideApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CityRepository cityRepository;

	@SpyBean
	private RouteRepository routeRepository;

    void contextLoads() {
    }

}
